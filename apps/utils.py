import ConfigParser


def load_config(absolute_filepath, key):

    config = ConfigParser.ConfigParser()
    config.read(absolute_filepath)
    if config.get('database', key):
        return config.get('database', key)
    else:
        return ''
